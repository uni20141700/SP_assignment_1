#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include "LRU.h"

//This function makes new node contatining item_.
//It will return the new node.
Node* newNode(int item_) {
    Node *node = (Node *) malloc(sizeof(Node));
    node->item = item_;
    node->next = NULL;
    node->prev = NULL;
    return node;
}

//This function makes new empty queue with size.
//It will return empty queue which can contain items up to size.
Queue* createQueue(int size) {
    Queue *queue_ = (Queue *) malloc(sizeof(Queue));
    queue_->queue_size = size;
    queue_->count_ = 0;
    queue_->head = NULL;
    queue_->tail = NULL;
    return queue_;
}

//This function will check whether queue is empty or not.
int isQueueEmpty(Queue* queue_) {
    return !queue_->count_;
}

//This function will remove last element(tail) in queue_.
void DeQueue(Queue* queue_) {
    Node *node = queue_->tail;
    if (node->prev) {
        node->prev->next = NULL;
    }
    queue_->tail = node->prev;
    free(node);
    queue_->count_--;
}

//This function roles add the node containing item_ in queue_ at head.
void EnQueue(Queue* queue_, int item_) {
    Node *node = newNode(item_);
    if (queue_->head) {
        queue_->head->prev = node;
    }
    node->next = queue_->head;
    queue_->head = node;
    queue_->count_++;
}

//This function mention item_ in queue. So the queue should be modified along LRU.
//If item_ is not in queue, it should be added,
//If item_ is in queue_, it should be placed in front of queue_.
void Reference(Queue* queue_, int item_) {
    Node *node, *node2;
    EnQueue(queue_, item_);
    node = queue_->head->next;
    while (node) {
        node2 = node->next;
        if (node->item == item_) {
            node->prev->next = node2;
            *(node2 ? &node2->prev : &queue_->tail) = node->prev;
            free(node);
            queue_->count_--;
            break;
        }
        node = node2;
    }
    if (!node && queue_->count_ > queue_->queue_size) {
        DeQueue(queue_);
    }
}

//This function clear the queue_.
void ClearQueue(Queue* queue_) {
    while (!isQueueEmpty(queue_)) {
        DeQueue(queue_);
    }
}

void PrintQueue(Queue* queue_) {
    Node* it = queue_->head;
    int index = 0;
    printf("------------------------\n");
    printf("Queue size : %d\n", queue_->count_);
    printf("Queue capacity : %d\n", queue_->queue_size);
    while (it != NULL) {
        printf("Queue %d Element : %d\n", index, it->item);
        index++;
        it = it->next;
    }
    printf("------------------------\n");
}
